/**
 * Created by carlos on 8/5/16.
 */

function printNumbers() {
  for (var i = 1; i <= 100; i++) {
    var num = "";
    if (i % 3 === 0) {
      num = "Wize";
    }
    if (i % 5 === 0) {
      num += "Line"
    }
    if (num === "") {
      num = i;
    }
    console.log(num)
  }
}

printNumbers();
/**
 * @param {number[]} digits
 * @return {number[]}
 */
var plusOne = function(digits) {
  var carry = 1;
  for (var i = digits.length -1; i >= 0; i--) {
    digits[i] += carry;
    if (digits[i] >= 10) {
      carry = (digits[i] - (digits[i] % 10)) / 10;
      digits[i] = digits[i] % 10;
    } else {
      carry = 0;
    }
  }
  if (carry > 0) {
    digits.unshift(carry);
  }
  return digits;
};


console.log(plusOne([1, 9, 8]));
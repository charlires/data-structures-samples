/**
 * Created by carlos on 7/26/16.
 */

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var findKthLargest = function(nums, k) {
  var sortedNums = sort(nums);
  if (1 <= k && k <= nums.length) {
    return sortedNums[nums.length - k]
  }
};

var sort = function (nums) {
  var swapped = true;
  var j = 0;
  var tmp = null;
  while (swapped) {
    swapped = false;
    j++;
    for (var i = 0; i < nums.length - j; i++) {
      if (nums[i] > nums[i + 1]) {
        tmp = nums[i];
        nums[i] = nums[i + 1];
        nums[i + 1] = tmp;
        swapped = true;
      }
    }
  }
  return nums;
};

console.log(findKthLargest([3, 2, 1, 5, 6, 7, 4, 1], 2));

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var singleNumber = function(nums) {

  var a = {};

  var singles = [];

  for (var i = 0; i < nums.length; i++) {
    if (a.hasOwnProperty(nums[i] + '')) {
      a[nums[i] + ''] ++;
    } else {
      a[nums[i] + ''] = 1;
    }
  }

  for (var key in a) {
    if (a[key] == 1) {
      singles.push(key * 1)
    }
  }

  return singles;
};


console.log(singleNumber([1, 2, 1, 3, 2, 5]));
'use strict'
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isIsomorphic = function(s, t) {
  var hashT = {};
  var hashS = {};
  if (s.length !== t.length) return false;
  for(let i in s) {
    var keyS = s[i];
    var keyT = t[i];
    if (hashS.hasOwnProperty(keyS)) {
      if (hashS[keyS] !== t[i]) return false;
    } else {
      hashS[keyS] = t[i];
    }

    if (hashT.hasOwnProperty(keyT)) {
      if (hashT[keyT] !== s[i]) return false;
    } else {
      hashT[keyT] = s[i];
    }

  }

  return true;
};

console.log(isIsomorphic("as", "aa"));
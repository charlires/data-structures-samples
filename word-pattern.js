'use strict'
/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var wordPattern = function(pattern, str) {
  var hashT = {};
  var hashS = {};
  var t = str.split(" ");
  if (pattern.length !== t.length) return false;
  for(let i in pattern) {
    var keyS = pattern[i];
    var keyT = t[i];
    if (hashS.hasOwnProperty(keyS)) {
      if (hashS[keyS] !== t[i]) return false;
    } else {
      hashS[keyS] = t[i];
    }

    if (hashT.hasOwnProperty(keyT)) {
      if (hashT[keyT] !== pattern[i]) return false;
    } else {
      hashT[keyT] = pattern[i];
    }

  }

  return true;
};

console.log(wordPattern("abbc", "dog cat cat dog"));
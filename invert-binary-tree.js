/**
 * Created by carlos on 8/5/16.
 */
/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var invertTree = function(root) {
  if (root !== null) {
    var temp = invertTree(root.left);
    root.left = invertTree(root.right);
    root.right = temp;
  }
  return root;
};
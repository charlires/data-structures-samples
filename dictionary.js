/**
 * Created by carlos on 8/5/16.
 */
/**
 * @param {number} n
 * @return {string}
 * 65 - 90
 */
var convertToTitle = function(n) {
  var word = "";
  var residual = (n % 26);
  var letters = (n - residual) / 26;
  if (residual === 0) {
    letters -= 1;
    residual = 26;
  }
  if (letters > 0) {
    word += convertToTitle(letters);
  }
  word += String.fromCharCode(residual + 64);
  return word;
};

console.log(convertToTitle(234234234));
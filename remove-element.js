/**
 * Created by carlos on 8/5/16.
 */
/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
var removeElement = function(nums, val) {
  var newLength = 0;
  while(nums.length > 0) {
    var num = nums.pop();
    if(num !== val) {
      newLength++;
    }
  }
  return newLength;
};

console.log(removeElement([3, 2, 2, 3, 2, 4, 4, 5, 6, 7], 3))
/**
 * Sumar uno a uno los digitos hasta que el resultado sea una sola sifra 
 */
var addDigits = function(number) {
  if (number < 10) {
    return number
  }
  var result = 0;
  while(number > 0) {
    var current = number % 10;
    result += current;
    number = (number-current)/10;
  }
  return addDigits(result);
};

console.log(addDigits(1235));
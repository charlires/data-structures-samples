/**
 * Created by carlos on 7/28/16.
 */
/**
 * @param {number} n
 * @return {number}
 */
var countNumbersWithUniqueDigits = function (n) {

  var dic = {};
  var validate = function (num) {
    for (var j = 1; j <= 9; j++) {
      if (num.indexOf(Array(2 + 1).join(j + '')) > -1) {
        return false;
      }
    }
    return true;
  };

  var allNumbers = [];

  for (var i = 0; i < Math.pow(10, n); i++) {
    if (validate(i + '')) {
      allNumbers.push(i);
    }
  }

  

  return allNumbers.length;
};

console.log(countNumbersWithUniqueDigits(3));
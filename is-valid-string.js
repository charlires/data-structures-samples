/**
 * Created by carlos on 7/27/16.
 */
var isValid = function(s) {
  var a = s.split('');
  var stack = [];
  for (var i = 0; i < a.length; i++) {
    if (stack.length === 0) {
      if ('})]'.indexOf(a[i]) > -1) {
        return false;
      }
    }
    if ('{(['.indexOf(a[i]) > -1) {
      stack.push(a[i]);
    } else {
      var e = stack.pop();
      var compare = 2;
      if (e === '(') {
        compare = 1;
      }
      if (a[i] !== String.fromCharCode(e.charCodeAt(0) + compare)) {
        return false;
      }
    }
  }
  return stack.length < 1;
};


var isValid = function(s) {
  var obj = {
    '{': '}',
    '(': ')',
    '[': ']'
  };
  var a = s.split('');
  var stack = [];

  for (var i = 0; i < a.length; i++) {
    var symbol = a[i];
    if (obj.hasOwnProperty(symbol)) {
      stack.push(symbol);
    } else {
      var temp = stack.pop();
      if (obj[temp] !== symbol) {
        return false;
      }
    }
  }
  return stack.length < 1;
};

console.log(isValid('[{}()[]]'));
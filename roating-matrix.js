/**
 * Created by carlos on 7/31/16.
 */

function roatingMatrix(m) {
  var m2 = matrix(5, 0);
  var n = m.length;
  for (var i = 0; i < n; i++) {
    for (var k = 0; k < n; k++) {
      m2[i][k] = m[n - k - 1][i];
    }
    // m2[i][n] = m[0][i];
    // m2[0][i] = m[n - i][0];
    // m2[n][n - i] = m[i][n];
    // m2[n - i][0] = m[n][n - i];
  }
  return m2;
}


function matrix(n, initial) {
  var i, j, a, mat = [];
  for (i = 0; i < n; i += 1) {
    a = [];
    for (j = 0; j < n; j += 1) {
      if (initial != null) {
        a[j] = initial;
      } else {
        a[j] = Math.floor((Math.random() * 99) + 1);
      }
    }
    mat[i] = a;
  }
  return mat;
}

var m1 = matrix(5);

console.log(m1);

console.log(roatingMatrix(m1, 5));
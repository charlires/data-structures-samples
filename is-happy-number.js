/**
 * Created by carlos on 7/27/16.
 */


/**
 * @param {number} n
 * @return {boolean}
 */
var isHappy = function(n) {
  if (n === 1 || n === 7) {
    return true;
  } else if (n < 10) {
    return false;
  }
  var result = 0;
  while(n > 0) {
    var current = n % 10;
    result += current * current;
    n = (n-current) / 10;
  }
  return isHappy(result);
};


// Using hash map this one fails on 1111111
var hash = {};
var isHappy = function(n) {
  if (n === 1) {
    return true;
  } else if (n < 10) {
    if (hash.hasOwnProperty(n)) {
      return false;
    } else {
      hash[n] = true;
    }
  }
  var result = 0;
  while(n > 0) {
    var current = n % 10;
    result += current * current;
    n = (n-current) / 10;
  }
  return isHappy(result);
};

console.log(isHappy(1111111));
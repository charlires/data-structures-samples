/**
 * Created by carlos on 8/5/16.
 */
/**
 * @param {number} n
 * @return {boolean}
 */
var isPowerOfTwo = function(n) {
  if (n ===1 ) {
    return true
  }
  while (n >= 2 ){
    n = (n/2);
    if (n == 1) {
      return true;
    }
  }
  return false;
};

console.log(isPowerOfTwo(5));
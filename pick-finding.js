/**
 * Created by carlos on 7/26/16.
 */


var pickBinarySearch = function (items) {
  var startIndex = 0;
  var stopIndex = items.length - 1;
  var middle = Math.floor((stopIndex + startIndex) / 2);
  while (startIndex < stopIndex) {
    if (items[middle + 1] >= items[middle] && items[middle] >= items[middle - 1]) {
      startIndex = middle + 1;
    } else if (items[middle + 1] <= items[middle] && items[middle] <= items[middle - 1]) {
      stopIndex = middle - 1;
    } else {
      break;
    }
    middle = Math.floor((stopIndex + startIndex) / 2);
  }
  return items[middle];
};

// console.log(pickBinarySearch([1, 2, 3, 4, 5, 6, 7, 8, 5, 5, 4, 3]));

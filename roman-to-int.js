'use strict'

/**
 * Translate roman numbers to decimal
 * @param {string} s
 * @return {number}
 */
var romanToInt = function(s) {
  var hash = {
    "I": 1,
    "V": 5,
    "X": 10,
    "L": 50,
    "C": 100,
    "D": 500,
    "M": 1000
  };

  let result = 0;
  for(let i = 0; i < s.length; i++) {
    let key1 = s[i];
    let key2 = s[i+1];
    if (hash[key2] && hash[key1] < hash[key2]) {
      result -= hash[key1];
    } else {
      result += hash[key1];
    }
  }
  return result;
};

console.log(romanToInt("MCMXCVI"));
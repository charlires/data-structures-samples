/**
 * Created by carlos on 7/28/16.
 */
function longestString(i) {

  // i will be an array.
  // return the longest string in the array

  var longest = i[0];
  for (var a = 0; a < i.length; a++) {
    if (longest.length < i[a].length) {
      longest = i[a];
    }
  }
  return longest;
}

console.log(longestString(['a','ab','abc']));
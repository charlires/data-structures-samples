'use strict';

/**
 * @param {string} s
 * @return {string}
 */
var reverseVowels = function(s) {
  var vowels = {
    "a": true,
    "e": true,
    "i": true,
    "o": true,
    "u": true
  };

  var stackVowels = [];

  s = s.split('');

  for (let i = 0; i < s.length; i++) {
    if (vowels.hasOwnProperty(s[i].toLowerCase())) {
      stackVowels.push(s[i]);
    }
  }

  for (let i = 0; i < s.length; i++) {
    if (vowels.hasOwnProperty(s[i].toLowerCase())) {
      s[i] = stackVowels.pop();
    }
  }

  return s.join('');
};


console.log(reverseVowels("holA"));
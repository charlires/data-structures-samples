'use strict';

function fillWithZeros(matrix) {
  var zerosX = {};
  var zerosY = {};

  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      if (matrix[i][j] === 0) {
        zerosX[i] = true;
        zerosY[j] = true;
      }
    }
  }

  // zerosX = {'1': true}
  // zerosY = {'1': true, '2': true}

  for (let l= 0; l< matrix.length; l++) {
    for (let k = 0; k < matrix[0].length; k++) {
      // l= 2
      // k = 2
      if (zerosX.hasOwnProperty(l+ '') || zerosY.hasOwnProperty(k + '')) {
        matrix[l][k] = 0;
      }
    }
  }
  return matrix;
}


// NxM
// [1, 1], [1, 2]
var matrix = [
  [1, 2, 3, 23,4],
  [4, 0, 0, 3, 4],
  [1, 3, 2, 1, 3]
];

console.log(fillWithZeros(matrix));

// result
//[
//  [1,0,0],
//  [0,0,0],
//  [1,0,0]
//]
/**
 * Created by carlos on 7/27/16.
 */
/**
 * @param {string} s
 * @return {string}
 */
var reverseString1 = function (s) {
  var len = s.length;
  if (len <= 1) {
    return s;
  } else {
    return reverseString1(s.substring(len / 2)) + reverseString1(s.substring(0, len / 2))
  }
};

var reverseString2 = function(s) {
  var str = "";
  s = s.split("");
  for (var i = s.length - 1; i >= 0; i--) {
    str += s.pop();
  }
  return str;
};

console.log(reverseString2("ASDFGHJK"));
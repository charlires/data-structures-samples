/**
 * Created by carlos on 7/26/16.
 */

var binarySearch = function (items, value) {
  var startIndex = 0;
  var stopIndex = items.length - 1;
  var middle = Math.floor((stopIndex + startIndex) / 2);

  while (items[middle] != value && startIndex < stopIndex) {
    if (value < items[middle]) {
      stopIndex = middle - 1;
    } else if (value > items[middle]) {
      startIndex = middle + 1;
    }
    middle = Math.floor((stopIndex + startIndex) / 2);
  }
  return (items[middle] != value) ? -1 : middle;
};

console.log(binarySearch([1, 2, 3, 4, 5, 6, 7, 8, 9], 6));


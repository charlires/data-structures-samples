/**
 * Created by carlos on 7/28/16.
 */
function arraySum(i) {
  // i will be an array, containing integers, strings and/or arrays like itself.
  // Sum all the integers you find, anywhere in the nest of arrays.
  return i.reduce(function(a, b) {
    if (a instanceof Array) {
      a = arraySum(a)
    } else if (b instanceof Array) {
      b = arraySum(b)
    }
    return a+b;
  }, 0);
}


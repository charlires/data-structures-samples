/**
 * Created by carlos on 7/27/16.
 */

function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}

var left = new TreeNode('left');
var right = new TreeNode('right');

var q = new TreeNode('p');
q.left = left;
q.right = right;

var p = new TreeNode('p');
p.left = left;
p.right = right;

/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {boolean}
 */
var isSameTree = function(p, q) {
  if (p==null && q==null) {
    return true;
  } else if (p==null || q==null) {
    return false;
  }

  if (p.val === q.val) {
    return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
  } else {
    return false;
  }
};

console.log(isSameTree(p, q));





'use strict';
// var lA = [1, 2, 3]
// var lB = [1, 2, 3, 4, 5] //sublist
var lA = [1, 2, 3]
var lB = [1, 2, 3] //equal
// A = [1, 2, 3, 4, 5]
// B = [2, 3, 4] //superlist
// A = [1, 2, 4]
// B = [1, 2, 3, 4, 5] //neither
// A = [1,2,3], B = [1,1,2,3,4,5]
// A = [3,4,5], B = [1,2,3,4,5]

var getStatus = function (A, B) {
  if(A.length === B.length) {
    //equal
    for(let i = 0; i < A.length; i++) {
      if (A[i] !== B[i]) {
        return 'neither';
      }
    }
    return 'equal';
  } else {
    var status = 'superlist';
    if (A.length < B.length) {
      status = 'sublist';
      var temp = A;
      A = B;
      B = temp;
    }
    let j = 0;
    for(let i = 0; i < A.length; i++) {
      if (B[j] === A[i]) {
        j++;
      } else {
        j = 0;
      }
      if (j > B.length) return status;
    }
    return 'neither';
  }
};

console.log(getStatus(lA, lB));

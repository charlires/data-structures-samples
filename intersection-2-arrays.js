/**
 * Created by carlos on 8/5/16.
 */
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var intersect = function(nums1, nums2) {

  var hash = {};
  var stack = [];
  var array1 = [];
  var array2 = [];

  if (nums1.length < nums2.length) {
    array1 = nums1;
    array2 = nums2;

  } else {
    array1 = nums2;
    array2 = nums1;
  }

  for (var i in array1) {
    var key = array1[i]+'';
    hash[key] = hash[key] || 0;
    hash[key]++;
  }

  for (var j in array2) {
    if (hash.hasOwnProperty(array2[j]) && hash[array2[j]] > 0) {
      stack.push(array2[j]);
      hash[array2[j]]--;
    }
  }

  return stack;

};

console.log(intersect([4,7,9,7,6,7], [5,0,0,6,1,6,2,2,4]));
/**
 * Created by carlos on 8/5/16.
 */
'use strict';
/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function(nums) {
  var majority = parseInt(nums.length/2);
  var hash = {};
  for (var i in nums) {
    var key = nums[i] + "";
    hash[key] = hash[key] || 0;
    hash[key] ++;
  }

  for(var j in hash) {
    if (hash[j] > majority) {
      return parseInt(j);
    }
  }
};

var majorityElement = function(nums) {
  var hash = {};
  if (nums.length == 1) {
    return nums[0];
  }
  for(let i = 0; i<nums.length; i++) {
    let key = nums[i] + '';
    hash[key] = hash[key] || 0;
    hash[key]++;
    if (hash[key] > nums.length/2) {
      return nums[i];
    }
  }
};

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var majorityElement = function(nums) {
  var stack = [];
  var hash = {};
  var stackHash = {};
  if (nums.length == 1) {
    return [nums[0]];
  }
  for(let i = 0; i<nums.length; i++) {
    let key = nums[i] + '';
    hash[key] = hash[key] || 0;
    hash[key]++;
    if (hash[key] > nums.length/3 && !stackHash[key]) {
      stackHash[key] = true;
      stack.push(nums[i]);
    }
  }
  return stack;
};

console.log(majorityElement([1, 2, 3, 4, 51, 1, 1, 1, 1, 1, 1]));
/**
 * Created by carlos on 8/5/16.
 */
/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function(nums) {
  var j = 0;
  for (var i = 0; i < nums.length; i++) {
    if (nums[i] !== 0) {
      var temp = nums[i];
      nums[i] = nums[j];
      nums[j] = temp;
      j++;
    }
  }
};

console.log(moveZeroes([0, 1, 0, 3, 12]));
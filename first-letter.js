/**
 * Created by carlos on 8/11/16.
 */
// 'abcdefghija' => b
// 'hello' => h
// 'Java' => J
// 'simplest' => i
// 'abA' => b

var getFirst = function (str) {
  var hash = {};

  for(let i = 0; i < str.length; i++) {
    let key = str[i].toLowerCase();
    hash[key] = hash[key] || 0;
    hash[key]++;
  }

  for(let i = 0; i < str.length; i++) {
    let key = str[i].toLowerCase();
    if (hash[key] == 1) {
      return key;
    }
  }
};

/**
 * Created by carlos on 8/9/16.
 */
/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */

function ListNode(val) {
    this.val = val;
    this.next = null;
}
var addTwoNumbers = function(l1, l2) {
  var doSum = function(n1, n2) {
    var sum = n1.val + n2.val;
    if (sum >= 10) {
      n2.val = (sum % 10);
      n2.next = n2.next || new ListNode(0);
      n2.next.val += 1;
    } else {
      n2.val = sum;
    }
    if (n1.next === null && n2.next === null) {
      return
    }
    n1.next = n1.next || new ListNode(0);
    n2.next = n2.next || new ListNode(0);
    doSum(n1.next, n2.next);
  };
  doSum(l1, l2);
  return l2;
};

var node1 = {
  val: 9,
  next: {
    val: 8,
    next: null
  }
};

var node2 = {
  val: 1,
  next: {
    val: 2,
    next: null
  }
};

console.log(JSON.stringify(addTwoNumbers(node1, node2)));